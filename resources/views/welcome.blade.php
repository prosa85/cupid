@extends('layouts.cupid')

@section('content')

                <div class="section__column">
                    <h2>Connect with an advisor.</h2>
                    <p>Our advisor matching program isn’t available yet, but join our waitlist and we’ll notify you as soon as we’re able to connect you with someone.</p>
                </div><!-- /.section__column -->
                <div class="section__column">
                    <div class="form flex">
                        <!--[if lte IE 8]>
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                        <![endif]-->

                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                        <script>
                            var params = {}
                            hbspt.forms.create({
                                portalId: "4826914",
                                formId: "6858762f-55a2-47d4-b0fb-7e2e4d1765a7",
                                onFormSubmit: function ($form) {
                                    console.log($form)
                                    var portfoliosizeCheck = $form.find("[name=portfolio_size]:checked").val()
                                    var size = 0
                                    if (portfoliosizeCheck == "$250,000 — $500,000" || portfoliosizeCheck == "$500,000 — $1,000,000") {
                                        size = 251000
                                    }
                                    if (portfoliosizeCheck == "More than $1,000,000") {
                                        size = 1001000
                                    }

                                    console.log(size)
                                    console.log($form.find("[name=email]").val())
                                    params = {
                                        "email": $form.find("[name=email]").val(),
                                        "portfolio_size": size,
                                        "portfolio": portfoliosizeCheck,
                                        "firstname": $form.find("[name=firstname]").val(),
                                        "lastname": $form.find("[name=lastname]").val(),
                                        "whatTypeOfHelp": $form.find("[name=what_type_of_help_are_you_looking_for_]").val(),
                                        "local_or_remota": $form.find("[name=do_you_prefer_local_or_remote_advice_]").val(),
                                        "zip": $form.find("[name=zip]").val(),
                                        "phone": $form.find("[name=phone]").val(),
                                    }

                                },
                                onFormSubmitted() {
                                    console.log('redirect now')
                                    window.location = "/lead?" + jQuery.param(params);

                                }

                            });
                        </script>

                        </div>
                    <p class="disclaimer">Information collected on this page may be used for statistical, advisor matching, marketing or other purposes, or may not be used at all. Any questions answered on this page should not be construed as suitability questions and will not be used to deliver investment advice or to make securities recommendations or advisor referrals. Any future advisor matching feature will not reference a list of customers of Altruist Financial LLC (an SEC-registered broker-dealer and member FINRA/SIPC) nor of Altruist LLC (an SEC-registered investment adviser).</p>
                </div><!-- /.section__column -->
@endsection
