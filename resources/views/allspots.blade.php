@extends('layouts.cupid')

@section('content')

        <div class="m-b-md">
                <form action="pick-advisors" method="POST">
                    <table style="width: 500px">
                        <tr>
                            <td>Select</td>
                            @if(env('APP_ENV')=='local')
                            <td>Spot id</td>
                            <td>Advisor ID</td>
                            @endif
                            <td>Advisor</td>
                            <td>Email</td>
                        </tr>
                            @csrf
                        @foreach($spots as $spot)
                            <tr>
                                <td><input type="checkbox" name="pick[]" value="{{$spot->id}}" @if($spot->lead_id) checked disabled @endif ></td>
                                @if(env('APP_ENV')=='local')
                                <td>{{$spot->id}}</td>
                                <td>{{$spot->advisor->id}}</td>
                                @endif
                                <td>{{$spot->advisor->firstname}} {{$spot->advisor->lastname}} </td>
                                <td>{{$spot->advisor->email}}</td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="submit-form"><input type="submit" value="Send" class="btn"></div>
                </form>
        </div>
        @if(env('APP_ENV')=='local')
        <div class="m-b-md">
            <div>All Advisors Spots</div>
            <table>
                <tr>
                    <td>Spot id</td><td>Advisor ID</td><td>Advisor</td><td>Email</td><td>Min</td><td>Active</td>
                </tr>
                @foreach($allSpots as $spot)
                    <tr @if($spot->status == 1) class="text-success" @else class="text-danger" @endif>
                        <td>{{$spot->id}}</td>
                        <td>{{$spot->advisor->id}}</td>
                        <td>{{$spot->advisor->firstname}} {{$spot->advisor->lastname}} </td>
                        <td>{{$spot->advisor->email}}</td>
                        <td>{{$spot->advisor->cupid_portfolio_minimum}}</td>

                        <td>
                            @if($spot->status == 1)
                                YES
                            @else
                                NO
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    @endif
@endsection
