<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cupid</title>

    <link rel="shortcut icon"
          href="https://altruist.com/wp-content/themes/new-altruist/dist/images/favicon.ico?ver=1589556126">
    <!-- Styles -->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <style>

    </style>

    <!-- Styles -->
    <style>
        .main:not(.main--careers) {
            height: auto;
        }
        .disclaimer {
            font-size: 12px;
            max-width: 637px;
            margin: 0;
            margin-top: 0px;
            color: #7e8385;

            line-height: 22px;
        }

    </style>
</head>
<body>
<div class="wapper">
    <header class="site-header header header-wrapper mega-menu-parent">

        <a href="https://altruist.com/" class="logo"></a>



        <nav class="nav">
            <ul id="menu-main-menu" class="">
                <li id="menu-item-360" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-360"><a
                        href="https://altruist.com/company/">About</a></li>
                <li id="menu-item-25" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25"><a
                        href="https://learn.altruist.com">Learn</a></li>
            </ul>
        </nav>


    </header>

    <div class="main main--application">

        <section class="section-application">
            <!--<a href="#" class="section__logo section__sticky"></a>
            <a href="#section-form" class="section__button btn btn-scroll section__sticky">Apply</a>-->

            <div class="section__columns page-center">
                <div class="flex">
                    @yield('content')
                </div>
            </div><!-- /.section__columns -->
        </section>
    </div>

    <section class="section-form" id="section-form">
        <div class="section__bottom">
            <nav class="nav-utilities">
                <ul id="menu-homepage-menu" class="">
                    <li id="menu-item-176"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-176"><a
                            href="https://altruist.com/contact/">Contact</a></li>
                    <li id="menu-item-654"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-654"><a
                            href="/careers">Careers</a></li>
                    <li id="menu-item-201"
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-201"><a
                            href="http://news.altruist.com">News</a></li>
                </ul>
            </nav>
            <div class="copyright">

                <nav class="nav-utilities">
                    <ul id="menu-privacy-menu" class="">
                        <li id="menu-item-564"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-564"><a
                                href="https://altruist.com/security/">Security</a></li>
                        <li id="menu-item-563"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-563"><a
                                href="https://altruist.com/legal/">Legal</a></li>
                        <li id="menu-item-205"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-205"><a
                                href="https://altruist.com/terms/">Terms</a></li>
                    </ul>
                </nav>
                <p>© 2020 Altruist Corp</p>
            </div><!-- /.copyright -->

        </div><!-- /.section__bottom -->    </section>
    <footer class="footer ">
        <div class="footer__cols">
            <div class="footer__col">
                <p>Altruist Corp (“Altruist”) offers a software engine that helps financial advisors achieve better
                    outcomes. Investment management and advisory services are provided by Altruist LLC, an
                    SEC-registered investment adviser, and brokerage related products are provided by Altruist Financial
                    LLC, a member of <a href="https://www.finra.org/#/">FINRA</a>/<a
                        href="https://www.sipc.org/">SIPC.</a> Commission-free trading means $0 commission trades placed
                    in brokerage accounts via web or mobile devices. Other fees may still apply. Please see the <a
                        href="/m/Altruist-Fee-Schedule.pdf" target="_blank" rel="noopener">Altruist Financial Fee
                        Schedule</a> to learn more.</p>
            </div><!-- /.footer__col -->

            <div class="footer__col">
                <p>By using this website, you understand the information being presented is provided for informational
                    purposes only and agree to our <a href="/terms/">Terms of Use</a> and <a href="/legal">Privacy
                        Policy</a>. Nothing in this communication should be construed as an offer, recommendation, or
                    solicitation to buy or sell any security. Additionally, Altruist or its affiliates do not provide
                    tax advice and investors are encouraged to consult with their personal tax advisors.</p>
            </div><!-- /.footer__col -->

            <div class="footer__col">
                <p>All investing involves risk, including the possible loss of money you invest, and past performance
                    does not guarantee future performance. Historical returns, expected returns, and probability
                    projections are provided for informational and illustrative purposes, and may not reflect actual
                    future performance. Clearing and custody of all securities are provided by Apex Clearing
                    Corporation.</p>
                <p>Check the background of Altruist Financial LLC on FINRA’s BrokerCheck:&nbsp;<a class="c-link"
                                                                                                  href="https://brokercheck.finra.org/"
                                                                                                  target="_blank"
                                                                                                  rel="noopener noreferrer">https://brokercheck.finra.org/</a>
                </p>
            </div><!-- /.footer__col -->
        </div><!-- /.footer__cols -->
    </footer>
</div>

</body>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>

<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4826914.js"></script>
<!-- End of HubSpot Embed Code -->
</html>
