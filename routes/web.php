<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Spot;


Route::get('/lead-test', function () {
    return App\Lead::first()->form;
});

Route::get('search', 'SpotController@search');
//Route::get('lead', 'LeadController@form');
//Route::get('test', 'LeadController@testGetEmail');
Route::post('pick-advisors', 'SpotController@pickSpot')->middleware('cors');
Route::get('pick-advisors', 'SpotController@pickSpot')->middleware('cors');

Route::prefix('cards')->group(function () {
    Route::get('advisor', 'AdvisorController@card');
    Route::get('lead', 'LeadController@card');
});
Route::get('advisor-form','FormController@getAdvisorForm')->middleware('cors'); // this is used to pull the form fields for the front end





//Route::get('get-my-spot','SpotController@getMySpot');



Auth::routes();
Route::get('/', function () {
    return redirect('https://altruist.com');//view('welcome');
});
Route::get('/home', 'AdvisorController@showAdvisors')->name('home')->middleware('auth');
//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('spots','SpotController@index')->name('spots');
//Route::get('test-migration','SpotController@webapp');

//Route::get('test-api','SpotController@updateContactInHubspot');
