<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users =[
            [
                'name' =>'Pablo Rosa',
                'email'=>'prosa85@yahoo.com',
                'password'=> bcrypt('qweasdzxc')
            ],
            [
                'name' => 'John Scianna',
                'email' => 'john@altruist.com',
                'password' =>bcrypt('ASdWdcErYH')
            ]
            ];
        collect($users)->each(function($user){

            $user = new User($user);
            $user->save();
        });
    }
}
