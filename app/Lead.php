<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $appends = ['fullName'];

    public function getFullNameAttribute()
    {
        return "{$this->first} {$this->last}";
    }

    protected $casts = [
        'form' => 'array'
    ];

    public function spots(){
        return $this->hasMany('App\Spot');
    }

}
