<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Spot extends Model
{
    //
    protected $appends = ['showExtra'];

    public function getShowExtraAttribute()
    {
        return false;
    }

    static function newAdvisor(Advisor $advisor)
    {
        $spot = new Spot;
        $spot->advisor_id = $advisor->id;
        $spot->save();
    }

    public function advisor()
    {
        return $this->belongsTo('App\Advisor');
    }
    public function lead()
    {
        return $this->belongsTo('App\Lead');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeGetTreeActives($query)
    {
        return $query->where('status', 1)->limit(3);
    }

    static function scopeGetSpotsForMinimum($query, int $minimum)
    {
        return $query->whereHas('advisor', function (Builder $query) use ($minimum) {$query->where('cupid_portfolio_minimum','<=', $minimum); });
    }

    static function createSpotForAdvisor($id){
        $spot = new Spot;
        $spot->advisor_id = $id;
        $spot->save();
        return $spot;
    }

}
