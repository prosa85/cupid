<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advisor extends Model
{
    protected $appends = ['fullName'];
    protected $hidden = ['email', 'portal_id'];
    public function getFullNameAttribute()
    {
        return "{$this->firstname} {$this->lastname}";
    }

    public function spots(){
        return $this->hasMany('App\Spot');
    }

    public function leads()
    {
        return $this->hasManyThrough('App\Lead', 'App\Spot');
    }

}
