<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Spot;
use App\Http\Controllers\helpers\ApiCalls;
use App\Http\Controllers\helpers\AdvisorsOperations;
use Illuminate\Support\Facades\Log;

class updateContactsHubspot extends Command
{
    use ApiCalls, AdvisorsOperations;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webapp:updateHubspot {count=10: The number of row I will get}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update contacts in hubspot';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $spots = Spot::where('needs_update_in_hubspot',true)
                ->limit($this->argument('count'))
                ->get();
        LOG:info('updateHubspot: updating '. $spots->count());
        dump('updateHubspot updating '. $spots->count());

        if($spots->count()>0){

                LOG::info('Running--updateContactsHubspot-- updating hubspot database with local data base ' . $spots->count());
                dump('Running--updateContactsHubspot-- updating hubspot database with local data base ' . $spots->count());

                $chunks = $spots->chunk(50,true);
                forEach($chunks as $chunk){
                    $this->updateContactsInBulk(collect($chunk->values(),true), ENV('APP_ENV'));
                    $this->flagAsHubspotUpdated($chunk);
                sleep(1);
            };
        }
        else{
            LOG::info('Running--updateContactsHubspot-- Nothing in the local db to update');
            dump('Nothing to update '. $spots->count());
        }
    }
}
