<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\helpers\ApiCalls;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\helpers\AdvisorsOperations;

class DropFromWaitlist extends Command
{
    use ApiCalls,AdvisorsOperations;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webapp:drop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'I will query the list to drop users from the list and update the local db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        {
            LOG::info('Runing WAITLIST_ID_TO_DROP_CONTACT');
            dump('Runing DropFromWaitlist');
            $listId = env('WAITLIST_ID_TO_DROP_CONTACT');

            $offset = -1;
            while ($offset != 0) {
                dump('calling API');
                $list = collect(json_decode($this->getContactList($listId, $offset, 100), true));

                $contacts = collect($list->get('contacts'));
                dump('dropping contacts '. $contacts->count());
                $contacts->each(function ($contact) {
                    $this->removeContact($contact);
                });
                $offset = $list['vid-offset'];
                sleep(3);
            }
        }
    }
}
