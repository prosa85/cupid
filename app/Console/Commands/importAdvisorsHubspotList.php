<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\helpers\ApiCalls;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\helpers\AdvisorsOperations;

class importAdvisorsHubspotList extends Command
{
    use ApiCalls,AdvisorsOperations;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webapp:importAdvisors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Advisors from list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        LOG::info('Runing WAITLIST_ID_CUPID_ADVISORS');
        dump('Runing WAITLIST_ID_CUPID_ADVISORS');
        $listId = env('WAITLIST_ID_CUPID_ADVISORS');

        $offset = -1;
        while ($offset != 0) {
            dump('calling API');
            $list = collect(json_decode($this->getContactList($listId, $offset, 100), true));

            $contacts = collect($list->get('contacts'));
            dump('adding new contacts '. $contacts->count());
            $contacts->each(function ($contact) {
                $this->createContact($contact);
            });
            $offset = $list['vid-offset'];
            sleep(3);
        }
    }
}
