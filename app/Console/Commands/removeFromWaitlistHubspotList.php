<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\helpers\ApiCalls;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\helpers\AdvisorsOperations;

class removeFromWaitlistHubspotList extends Command
{

    use ApiCalls,AdvisorsOperations;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webapp:removeFromWaitList';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove from wait list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        {
            LOG::info('Runing WAITLIST_ID_TO_REMOVE_SPOTS');
            dump('Ruuning WAITLIST_ID_TO_ADD_NEW_SPOTS');
            $listId = env('WAITLIST_ID_TO_REMOVE_SPOTS');
            $droplistId = env('WAITLIST_ID_TO_DROP_CONTACT');

            $offset = -1;
            while ($offset != 0) {
                dump('calling API');
                $list = collect(json_decode($this->getContactList($listId, $offset, 100), true));
//                dump($list);
                $contacts = collect($list->get('contacts'));
                dump('Removing from waitlist, contacts '. $contacts->count());
                $contacts->each(function ($contact) {
                    $this->completeWaitList($contact);
                });
                $offset = $list['vid-offset'];
                sleep(2);
            }

            // removed dropped users
            $offset = -1;
            while ($offset != 0) {
                dump('calling API');
                $list = collect(json_decode($this->getContactList($droplistId, $offset, 100), true));

                $contacts = collect($list->get('contacts'));
                dump('Dropping contacts '. $contacts->count());
                $contacts->each(function ($contact) {
                    $this->completeWaitList($contact);
                });
                $offset = $list['vid-offset'];
                sleep(2);
            }
        }
    }
}
