<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel

{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\updateContactsHubspot',
        'App\Console\Commands\importAdvisorsHubspotList',
        'App\Console\Commands\removeFromWaitlistHubspotList',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
//        $schedule->command('webapp:importUpdateWebappWithNewSpots')
//            ->everyMinute()
//            ->after(function () {
//                \Artisan::call('webapp:updateHubspot 300');
//            })
//            ->unlessBetween('23:00', '23:50');



//        $schedule->command('webapp:importUpdateWebappWithNewSpots')
//            ->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
