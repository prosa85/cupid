<?php

namespace App\Http\Controllers;

use App\Advisor;
use App\Http\Controllers\helpers\ContactsHelper;
use App\Lead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;


class LeadController extends Controller
{

    use ContactsHelper;
    private $BASE_URL ="https://app.hubspot.com/contacts/4826914/contact/";



    public function testGetEmail(Request $request){
        $contact =  $this->getVidFromEmail($request->email);
        dd($contact);
    }

    public function card(Request $request){
        Log::info("lead". $request->associatedObjectId);

        $lead = Lead::where('vid', $request->associatedObjectId)->first();

        if(!$lead){
            return ["results" => [] ];
        }
//        dd($lead);
        $lead->load('spots');
        $lead->spots->load('advisor');
//        dd($lead);
        $response = collect([]);

        $lead->spots->each(function ($spot) use($response){
            if($spot->advisor){
//                dump($spot->lead);
                $lead = [
                    "objectId" => $spot->advisor->id,
                    "title" => "Advisor",
                    "advisor_name" => $spot->advisor->fullName,
                    "email" => $spot->advisor->email,
                    "date" => $spot->updated_at->toFormattedDateString(),

                ];

                if($spot->advisor->vid){
                    $lead["contact_link"] = $this->BASE_URL . $spot->advisor->vid;
                }
                $response->push($lead);
            }

        });

        Log::info($response->toJson());
        $results = [
            "results" => $response
        ];

        return response()->json($results);

    }
}
