<?php

namespace App\Http\Controllers;

use App\Advisor;
use App\Console\Commands\updateContactsHubspot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AdvisorController extends Controller
{

    private $BASE_URL ="https://app.hubspot.com/contacts/4826914/contact/";

    /**
     * @param Request $request
     * This function will pull data to feed the
     * card info in the crm
     * I will get object id and email
     *
     * Response needs to have
     * lead name, email and date assigned
     */
    public function showAdvisors(){
        return Advisor::all();
    }
    public function card(Request $request){
        Log::info("advisor". $request->associatedObjectId);

        $advisor = Advisor::where('vid', $request->associatedObjectId)->first();
        if(!$advisor){
            return ["results" => [] ];
        }

        $advisor->load('spots');
        $advisor->spots->load('lead');
//        dd($advisor);
        $response = collect([]);

        $advisor->spots->each(function ($spot) use($response){
            if($spot->lead){
//                dump($spot->lead);
                $lead = [
                   "objectId" => $spot->lead->id,
                   "title" => "Lead",
                   "lead_name" => $spot->lead->fullName,
                   "email" => $spot->lead->email,
                   "date" => $spot->updated_at->toFormattedDateString(),

               ];

                if($spot->lead->vid){
                    $lead["contact_link"] = $this->BASE_URL . $spot->lead->vid;
                }
                $response->push($lead);
            }

        });

        Log::info($response->toJson());
        $results = [
            "results" => $response
        ];

        return response()->json($results);

    }
}
