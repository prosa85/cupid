<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Controllers\helpers\ApiCalls;

class FormController extends Controller
{
    use ApiCalls;
    public function getAdvisorForm(){
        $fields = collect();
        $formFieldGroups = collect(json_decode($this->getFormByIdFromHubspot('0040f958-2a5e-44f1-842c-c49f3675c1fb'),true));
        $response = collect($formFieldGroups->get('formFieldGroups'));
        $response->each(function ($item, $key) use($fields){
            $f = [
                "id" => $key + 1,
                "name" => $item['fields'][0]['name'],
                "label"  => $item['fields'][0]['label'],
                "type" => $item['fields'][0]['type'] ,
                "fieldType" => $item['fields'][0]['fieldType'] ,
                "required" => $item['fields'][0]['required'],
                "options" => collect($item['fields'][0]['options'])

            ];

//            dump($f);
            $fields->push($f) ;
//            dump($fields);
        });

        return response($fields)
                ->header("Access-Control-Allow-Origin", config('cors.allowed_origins'))
                ->header("Access-Control-Allow-Methods", config('cors.allowed_methods'));
    }
}

