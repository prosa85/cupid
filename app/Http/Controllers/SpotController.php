<?php

namespace App\Http\Controllers;

use App\Advisor;
use App\Http\Controllers\helpers\ContactsHelper;
use App\Lead;
use App\Spot;
use Hamcrest\Core\IsNotTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\helpers\ApiCalls;
use App\Http\Controllers\helpers\AdvisorsOperations;
use Illuminate\Support\Facades\Session;

class SpotController extends Controller
{
    use ContactsHelper;
    use AdvisorsOperations;
    const BETA_CANDIDATES_LIST = 63;
    const ALL_USER_PRODUCTS_LIST = 238;

    public function __construct()
    {
        $this->middleware('auth')->only('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spots = Spot::where('status',true)
                ->orderBy('id')->get();

        return view('allspots')->with('spots', $spots);
    }

    /**
     * get the list of selected spots and assign the leads
     * @param Request $request
     * @return string
     */
     public function pickSpot(Request $request){
        $spot = Spot::find($request->id);
        $lead = Lead::find($request->lead_view_id);
        $spot->lead_id = $lead->id;
        $spot->save();

        $advisors = Advisor::where('id', $spot->advisor_id)->get();

        //update advisor with lead data

        $this->updateContactsInBulk($advisors, $lead);

//        Log::info([$advisors,$spots->load('lead')]);

         return response("thanks for submitting")
            ->header("Access-Control-Allow-Origin", "*")
            ->header("Access-Control-Allow-Methods", "post");

     }

    /***
     * @param $spots
     * grab the spots set status to 0 and create new spots
     */

    static function rotateSpots($spots, $lead){

            $spots->each(function($item) use ($lead) {
                $item->status=0;
                $item->lead_view_id= $lead->id;
                $item->save();
                Spot::createSpotForAdvisor($item->advisor_id);
            });
    }


    public function search(Request $request){

        $lead = $this->form($request);
        $min = $lead->portfolio_size;

        $spots = Spot::where('lead_view_id', $lead->id)
            ->whereBetween('created_at', [
                \carbon\Carbon::now()->subdays(env('DAY_LIMIT'))->format('Y-m-d'),
                \carbon\Carbon::now()->addday(1)->format('Y-m-d')
            ])->get();
//        $hasSpots = Spot::where('lead_view_id', $lead->id)->get();
        if($spots->isEmpty()){
            $spots = Spot::getSpotsForMinimum($min)->getTreeActives()->get()->load('advisor');
            $this->rotateSpots($spots, $lead);
        }
//        $allSpots = Spot::get()->load('advisor');

        return response($spots->load('advisor'))
            ->header("Access-Control-Allow-Origin", "*")
            ->header("Access-Control-Allow-Methods", "get");

//        return view('allspots', compact('spots', 'allSpots'));
    }

    public function form(Request $request) {
//        dump($request->all());
        $vid = $this->getVidFromEmail($request->email['value']);

        $lead = Lead::where('vid', $vid)
            ->orWhere('email', $request->email['value'])->first();

        if(!$lead) {
            $lead = new Lead;
        }

        $lead->first = $request->firstname['value'];
        $lead->last = $request->lastname['value'];
        $lead->portfolio_size = $request->size;
        $lead->vid = $vid;
        $lead->email = $request->email['value'];
        $lead->portfolio = $request->portfolio_size['value'];
        $lead->form = $request->all();
        $lead->save();

//        Session::put('lead', $lead);


        return $lead;
    }

}
