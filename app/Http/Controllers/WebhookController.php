<?php

namespace App\Http\Controllers;

use App\Advisor;
use App\Http\Controllers\helpers\AdvisorsOperations;
use App\Http\Controllers\helpers\ContactsHelper;
use App\Spot;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
use ContactsHelper, AdvisorsOperations;

public function webhook(Request $request ){
    Log::debug($request->all());
    collect($request->all())->each(function($item){
        Log::info($item['propertyName']." -" .$item['propertyValue']);

        $contact = Advisor::where('vid', $item['objectId'])->first();

        if($contact) {
            Log::info('contact found');
            if ($item['propertyName'] === 'cupid_portfolio_minimum') {
                $contact->cupid_portfolio_minimum = (int) $item['propertyValue'];
                Log::info("Updating min to -" .$item['propertyValue']);
            }
            if ($item['propertyName'] === 'firstname') {
                $contact->firstname = $item['propertyValue'];
                Log::info("Updating min to -" .$item['propertyValue']);
            }
            if ($item['propertyName'] === 'lastname') {
                $contact->lastname =  $item['propertyValue'];
                Log::info("Updating min to -" .$item['propertyValue']);
            }
            if ($item['propertyName'] === 'advisor_headshot') {
                $contact->advisor_headshot = $item['propertyValue'];
                Log::info("Updating min to -" .$item['propertyValue']);
            }
            if ($item['propertyName'] === 'company') {
                $contact->company = $item['propertyValue'];
                Log::info("Updating min to -" .$item['propertyValue']);
            }
            if ($item['propertyName'] === 'cupid_bio_additional') {
                $contact->cupid_bio_additional = $item['propertyValue'];
                Log::info("Updating min to -" .$item['propertyValue']);
            }
            if ($item['propertyName'] === 'cupid_bio') {
                $contact->cupid_bio = $item['propertyValue'];
                Log::info("Updating min to -" .$item['propertyValue']);
            }
            $contact->save();

            if ($item['propertyName'] === 'cupid_member' && $item['propertyValue'] === 'false') {
                //
                //remove all the spots for this user
                $contact->spots()->each(function($spot){
                   if($spot->status){
                       Log::info('delete spot'. $spot->id);
                       $spot->delete();
                   }
                });
                    $contact->active_advisor = false;
                    $contact->save();
            }else {
                $spotsForAdvisor = Spot::where([['advisor_id', $contact->id],['status', true]])->get();
                Log::info('find spots, we have '. $spotsForAdvisor->count());
                if($spotsForAdvisor->isEmpty()){
                    $spot = Spot::createSpotForAdvisor($contact->id);
                    Log::info('create a new spot');
                    Log::info($spot);
                    $contact->active_advisor = true;
                    $contact->save();
                }
            }

        } elseif($item['propertyName'] === 'cupid_member' && $item['propertyValue'] === 'true'){
            // create advisor, pull the data using the api and the vid
            $contactHs = $this->getContactByVid($item['objectId']);
//                Log::info($contactHs);
            $this->createContact($contactHs);
            Log::info('Creating new advisor');

        }

    });

    return response()->json("all good",200);
}


}
