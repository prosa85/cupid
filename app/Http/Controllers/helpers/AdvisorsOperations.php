<?php


namespace App\Http\Controllers\helpers;


use App\Advisor;
use App\Spot;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

trait AdvisorsOperations
{

    public function flagAsHubspotUpdated(Collection $spots){
        $spots->each(function($spot){
           $spot->needs_update_in_hubspot = false;
           $spot->save();
        });
    }

    public function createContact($contact, $offset=false)
    {
//        dump($contact);
        $email = $contact['properties']['email']['value'];
        //$advisor = $this->findSpotByEmail($email, true);
        $advisor = $this->findAdvisorbyVid($contact['vid']);
//        dd($contact);
        if(!$advisor){
            $advisor = new Advisor;
            $advisor->vid = $contact['vid'];
            $advisor->portal_id = $contact['portal-id'];
            $advisor->email = $contact['properties']['email']['value'];
            $advisor->firstname = (array_key_exists("firstname",$contact['properties'])? $contact['properties']['firstname']['value'] : '--');
            $advisor->lastname = (array_key_exists("lastname",$contact['properties'])? $contact['properties']['lastname']['value'] : '--');
            $advisor->company = (array_key_exists("company",$contact['properties'])? $contact['properties']['company']['value'] : '--');
            $advisor->advisor_headshot = (array_key_exists("advisor_headshot",$contact['properties'])? $contact['properties']['advisor_headshot']['value'] : '--');
            $advisor->cupid_bio = (array_key_exists("cupid_bio",$contact['properties'])? $contact['properties']['cupid_bio']['value'] : '--');
            $advisor->cupid_bio_additional = (array_key_exists("cupid_bio_additional",$contact['properties'])? $contact['properties']['cupid_bio_additional']['value'] : '--');
            $advisor->cupid_portfolio_minimum = (array_key_exists("cupid_portfolio_minimum",$contact['properties'])? $contact['properties']['cupid_portfolio_minimum']['value'] : 0);
            $advisor->save();
            Spot::newAdvisor($advisor);
            //$advisor->in_wait_list = 1; // add to the wait list
            //$advisor->spotNumber = $this->getLastSpotNumber($advisor);
    //        dump($advisor);
        }
        else {
            $advisor->cupid_portfolio_minimum = (array_key_exists("cupid_portfolio_minimum",$contact['properties'])? $contact['properties']['cupid_portfolio_minimum']['value'] : '--');
            $advisor->save();
        }
        dump('current contact '. $advisor->email);
        return $advisor;
    }

    public function findAdvisorbyVid($vid) {

             return Advisor::where('vid', $vid)->first();

    }

}
