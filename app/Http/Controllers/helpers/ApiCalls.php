<?php


namespace App\Http\Controllers\helpers;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;


Trait ApiCalls
{
    public function updateContactInHubspot($email = "prosa85@yahoo.com", $spot = 5)
    {
        $url = "https://api.hubapi.com/contacts/v1/contact/email/" . $email . "/profile?hapikey=" . env('HUBSPOT_PRO_KEY');
        $client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),)); //GuzzleHttp\Client
        $request = [
            'properties' => [
                [
                    'property' => 'wait_list_number',
                    "value" => $spot
                ]
            ]
        ];

        try {
            $result = $client->post($url, ['body' => json_encode($request)]);
            Log::info('Spot updated in hubspot ' . $email . ' with number ' . $spot);
        } catch (Exception $e) {
            dd($e);
        }

        return $result->getStatusCode();


    }

    public function getContactList($listId = 18, $vidOffset=0, $count = 100)
    {

        $query =[
            'propertyMode' => 'value_only',
            'count'=>$count,
            'property'=>'email',
            'hapikey'=> env('HUBSPOT_PRO_KEY')
        ];

        if($vidOffset>0){
            $query['vidOffset']=$vidOffset;
        }

        $url = "https://api.hubapi.com/contacts/v1/lists/" . $listId . "/contacts/all?" . http_build_query($query).'&property=cupid_portfolio_minimum&property=firstname&property=lastname&property=company&property=advisor_headshot&property=cupid_bio_additional&property=cupid_bio';

        dump($url);
        $client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),)); //GuzzleHttp\Client

        try {
            $result = $client->get($url);
            Log::info('getting hubspot list with id ' . $listId);
        } catch (Exception $e) {
            dd($e);
        }

        return $result->getBody();


    }

    public function getContactByEmailFromHubspot($email)
    {

//        $query =[
//            'propertyMode' => 'value_only',
//            'count'=>$count,
//            'property'=>'email',
//            'hapikey'=> env('HUBSPOT_PRO_KEY')
//        ];
//
//        if($vidOffset>0){
//            $query['vidOffset']=$vidOffset;
//        }

        $url = "https://api.hubapi.com/contacts/v1/contact/email/". $email ."/profile?hapikey=". env('HUBSPOT_PRO_KEY');
        //$url = "https://api.hubapi.com/contacts/v1/lists/" . $listId . "/contacts/all?" . http_build_query($query).'&property=cupid_portfolio_minimum&property=firstname&property=lastname';
//        dump($url);
        $client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),)); //GuzzleHttp\Client

        try {
            $result = $client->get($url);

        } catch (Exception $e) {
            dump('error', $e);
        }

        return $result->getBody();


    }

    public function getContactByVidFromHubspot($vid)
    {
        ///contacts/v1/contact/vid/:vid/profile
        $url = "https://api.hubapi.com/contacts/v1/contact/vid/". $vid ."/profile?hapikey=". env('HUBSPOT_PRO_KEY');
        //$url = "https://api.hubapi.com/contacts/v1/lists/" . $listId . "/contacts/all?" . http_build_query($query).'&property=cupid_portfolio_minimum&property=firstname&property=lastname';
        Log::info($url);
        $client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),)); //GuzzleHttp\Client

        try {
            $result = $client->get($url);

        } catch (Exception $e) {
            dump('error', $e);
        }
        return $result->getBody();
    }

    public function updateContactsInBulk($contacts, $lead){

//        @docs https://developers.hubspot.com/docs/methods/contacts/batch_create_or_update
        $key = env('HUBSPOT_PRO_KEY');

        LOG::info('Update advisors in hubspot '. $contacts->count());
//        dd("STOP");
        $client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false),'headers' => [ 'Content-Type' => 'application/json' ])); //GuzzleHttp\Client
        $url = 'https://api.hubapi.com/contacts/v1/contact/batch/?hapikey='. $key;

        $hsContacts = $contacts
            ->transform (function($contact) use($lead)
            {
                return $this->mapContactToHubspot($contact, $lead);
            });

        try {
            //dd($hsContacts->toJson());
            $result = $client->post($url, ["body" => $hsContacts->toJson()]);

        } catch (Exception $e) {
            dd($e);
        }

        return $hsContacts;

    }

//        hub keys
//        on_wait_list
//        wait_list_id
//        wait_list_spot
//        waitlist_webapp_control
//            'add to wait list',
//            'In wait list',
//            'remove from wait list',
//            'removed'
    public function mapContactToHubspot($advisor, $lead){
        $lastLead =
//            "<strong>Name:</strong> {$lead->fullName} <br>".
//            "<strong>Email:</strong> {$lead->email} <br>".
//            "<strong>Phone:</strong> {$lead->form['phone']} <br>".
//            "<strong>Portfolio Size:</strong>". $lead->portfolio . "<br>".
//            "<strong>Type of Help:</strong> {$lead->form['whatTypeOfHelp']} <br>".
//            "<strong>Local or Remote:</strong> {$lead->form['local_or_remota']}"
            "<strong>Name: </strong> {$lead->fullName}<br>".
            "<strong>{$lead->form['phone']['label']}: </strong> {$lead->form['phone']['value']}<br>".
            "<strong>{$lead->form['email']['label']}: </strong> {$lead->form['email']['value']}<br>".
            "<strong>{$lead->form['income_level']['label']}: </strong> {$lead->form['income_level']['value']}<br>".
            "<strong>{$lead->form['top_priority']['label']}: </strong> {$lead->form['top_priority']['value']}<br>".
            "<strong>{$lead->form['stage_of_life']['label']}: </strong> {$lead->form['stage_of_life']['value']}<br>".
            "<strong>{$lead->form['portfolio_size']['label']}: </strong> {$lead->form['portfolio_size']['value']}<br>".
            "<strong>{$lead->form['additional_info']['label']}: </strong> {$lead->form['additional_info']['value']}<br>".
            "<strong>{$lead->form['have_an_advisor']['label']}: </strong> {$lead->form['have_an_advisor']['value']}<br>".
            "<strong>{$lead->form['monthly_savings']['label']}: </strong> {$lead->form['monthly_savings']['value']}<br>".
            "<strong>{$lead->form['marital_status_cupid']['label']}: </strong> {$lead->form['marital_status_cupid']['value']}<br>".
            "<strong>{$lead->form['retirement_timeframe']['label']}: </strong> {$lead->form['retirement_timeframe']['value']}<br>"
       ;

        $hsContact=[
            'email' =>$advisor->email,
            'vid' => $advisor->vid,
            'properties' =>[
                [
                    'property'=>'last_assigned_lead',
                    'value'=> $lastLead
                ],[
                    'property'=>'cupid_webpp_control',
                    'value'=> "Send Lead email"
                ],

            ],
        ];

        return $hsContact;
    }

    public function getFormByIdFromHubspot($formId)
    {
        ///contacts/v1/contact/vid/:vid/profile
        /// https://api.hubapi.com/forms/v2/forms/1eae3941-9ce8-411d-8b91-87a92c0c9d5c/?hapikey=demo
        $url = "https://api.hubapi.com/forms/v2/forms/". $formId ."/?hapikey=". env('HUBSPOT_PRO_KEY');
        //$url = "https://api.hubapi.com/contacts/v1/lists/" . $listId . "/contacts/all?" . http_build_query($query).'&property=cupid_portfolio_minimum&property=firstname&property=lastname';
        Log::info($url);
        $client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),)); //GuzzleHttp\Client

        try {
            $result = $client->get($url);

        } catch (Exception $e) {
            dump('error', $e);
        }
        return $result->getBody();
    }

}
