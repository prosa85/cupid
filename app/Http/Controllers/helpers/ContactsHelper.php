<?php


namespace App\Http\Controllers\helpers;


trait ContactsHelper
{
 use ApiCalls;

 public function getHsContactByEmail($email){
     sleep(4);
     return collect(json_decode($this->getContactByEmailFromHubspot($email)));

 }

 public function getVidFromEmail($email){
    return $this->getHsContactByEmail($email)['vid'];
 }

 public function getContactByVid($vid){
     return collect(json_decode($this->getContactByVidFromHubspot($vid),true));
 }

}
